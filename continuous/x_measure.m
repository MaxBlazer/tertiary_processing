function z = x_measure(sensor, x_true, max_time)
    z = [];
    sensor.init_state();
    
    noise_model = false;
    if length(x_true) > 1
        noise_model = true;
    end
    
    i = 1;
    for current_time = 0:sensor.dt:max_time
        x_current = [];
        
        if noise_model
            x_current = get_x_true(x_true, current_time);
        else
            F = state_transition(current_time - x_true.time);
            x_current = x_true.state * F';
        end
        
        if sensor.is_it_see(x_current)
            z(i).time = current_time + sensor.get_delay(x_current);
            z(i).cov = sensor.error;
            z(i).state = mvnrnd(x_current, z(i).cov);
            z(i).true = x_current;

            i = i + 1;
        end
        
        sensor.next_state();        
    end
end