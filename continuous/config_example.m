% % parabolic tracks
% x0(1).state = [-23500 200 1 12000 200 -1];
% x0(2).state = [-20000 210 1 11000 205 -1];

% % parallel far tracks
% x0(1).state = [-24000 216 0 42000 -125 0];
% x0(2).state = [-20000 213 0 46000 -127 0];

% % almost parallel intersecting tracks
% x0(1).state = [-34000 216 0 52000 -125 0];
% x0(2).state = [-32000 203 0 53000 -137 0];

% x0(1).time = 0; % sec
% x0(2).time = 0; % sec

% sensor(1) = Sensor;
% sensor(1).error = diag([40 10 10 40 10 10].^2); % covariation
% sensor(1).polar_error = diag([120 4e-3].^2); % dist, azim
% sensor(1).dt = 3e-3; % sec
% sensor(1).pos = [0 0];
% sensor(1).azimuth_start = deg2rad(45);
% sensor(1).azimuth_end = deg2rad(135);
% sensor(1).azimuth_step = deg2rad(0.5);
% sensor(1).miss = 0.1;
% sensor(1).color(1,:) = [1 0.7 0]; % track 1
% sensor(1).color(2,:) = [0 0.8 0.9]; % track 2
% 
% sensor(2) = Sensor;
% sensor(2).error = diag([55 12 12 55 12 12].^2); % covariation
% sensor(2).polar_error = diag([150 4.5e-3].^2); % dist, azim
% sensor(2).dt = 3.5e-3; % sec
% sensor(2).pos = [2500 1000];
% sensor(2).azimuth_start = deg2rad(65); 
% sensor(2).azimuth_end = deg2rad(155); 
% sensor(2).azimuth_step = deg2rad(0.5); 
% sensor(2).miss = 0.1;
% sensor(2).color(1,:) = [1 0.2 0];
% sensor(2).color(2,:) = [0 0 1];
% 
% tracks_count = length(x0);
% sensors_count = length(sensor);
% 
% max_time = 200; % sec