% flatten measurments into one array, corresponding to their timings
function out = flatten_measures(z)
    out = [];

    while true
        min = inf;
        min_data = NaN;

        for s = 1:size(z, 1)
            for l = 1:size(z, 2)
                if ~isempty(z{s,l}) & z{s,l}(1).time < min
                    min = z{s,l}(1).time;
                    min_data = z{s,l}(1);
                    min_data.sensor = s;
                    min_data.track = l;
                end
            end
        end

        if min == inf
            break
        end

        z{min_data.sensor, min_data.track}(1) = [];

        out = cat(1, out, min_data);
    end
end