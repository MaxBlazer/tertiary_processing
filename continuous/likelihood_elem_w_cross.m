% z = [z{1}(l1) z{2}(l2) ... z{n}(ln)]
% z = [z(1) ... z(n)]

function out = likelihood_elem_w_cross(z)
    sensors_count = length(z);
    
    if sensors_count < 2
        error("First dimension of x (%d) is less than two.\nTo get likelihood element, 2 or more estimates needed.", sensors_count);
        return
    end
    
    % delta = [deltaN1' ... delta21']'
    delta = [];
    
    for i = 2:sensors_count
        delta = cat(2, z(i).state - z(1).state, delta);
    end
    
    % only for s = 2 (diagonal block only)
    delta_cov = sum_with_crosscov(z(1).cov, z(2).cov);
    
    out = mvnpdf(delta, 0, delta_cov);
end