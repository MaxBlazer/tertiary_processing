function print_accuracy(assoc)
    accuracy = 0;
    for i = 1:length(assoc)
        if assoc(i).success
            accuracy = accuracy + 1;
        end
    end    

    fprintf("association accuracy: %4.1f%% (%d misses)\n",...
        accuracy / length(assoc) * 100, length(assoc) - accuracy);
end