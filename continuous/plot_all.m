function plot_all(x_true, z, max_time, sensor, assoc, flat)
    tracks_count = length(x_true);
    sensors_count = length(sensor);
    
    x0 = struct('state', [], 'time', 0);
    noise_model = false;
    if isa(x_true, 'cell')
        for i = 1:tracks_count
            x0(i) = x_true{i}(1);
        end
        noise_model = true;
    else
        x0 = x_true;
    end
    
    % x0
    % x_true
    
    plot_meas_cov = true;
    plot_fus_cov = true;
    plot_meas_true_link = false;
    plot_assoc_triangles = false;
    
    % ---------------------------- sensors FOV ----------------------------
    for s = 1:sensors_count
        len = 1e5;
        
        % x = north, y = east 
        % => azimuth is like math angle
        x = [len*cos(sensor(s).azimuth_start); 0; len*cos(sensor(s).azimuth_end)];
        y = [len*sin(sensor(s).azimuth_start); 0; len*sin(sensor(s).azimuth_end)];
        
        x = x + sensor(s).pos(1);
        y = y + sensor(s).pos(2);
        
        plot(x, y,...
                 'Color', 'black',...
                 'LineStyle', '--',...
                 'LineWidth', 0.5,...
                 'Marker', 'none');
        hold on
    end

    % ---------------------- true tracks (wo noise) -----------------------
    true_track_color(1,:) = [0.5 1 0];
    true_track_color(2,:) = [0 1 0.5];
    
    track_plots = [];
    
    for l = 1:tracks_count
        F_start = state_transition(-x0(l).time);
        F_end = state_transition(max_time - x0(l).time);
        
        x_start = x0(l).state * F_start';
        x_end = x0(l).state * F_end';
        
        x = [x_start(1); x_end(1)];
        y = [x_start(4); x_end(4)];
        
        track_plots(l) = plot(x, y,...
                             'Color', true_track_color(l,:),...
                             'LineStyle', '-',...
                             'LineWidth', 1,...
                             'Marker', 'none');
    end
    
    % --------------------- true tracks (with noise) ---------------------- 
    if noise_model
        for l = 1:tracks_count        
            x = [];
            y = [];
            for i = 1:length(x_true{l})
                x = cat(1, x, x_true{l}(i).state(1));
                y = cat(1, y, x_true{l}(i).state(4));
            end

            plot(x, y,...
                    'Color', true_track_color(l,:),...
                    'LineStyle', '-',...
                    'LineWidth', 2,...
                    'Marker', 'none');
        end
    end
    % ---------------------------- measurements ---------------------------
    measure_plots = [];
    
    for s=1:sensors_count
        for l=1:tracks_count 
            color = sensor(s).color(l,:);
            
            x = [];
            y = [];
            
            for k = 1:length(z{s,l})
                x = cat(1, x, z{s,l}(k).state(1)); % extract x coordinate
                y = cat(1, y, z{s,l}(k).state(4)); % extract y coordinate
                
                if plot_meas_cov
                    error_ellipse(  x(k), y(k),...
                                    z{s,l}(k).cov([1 4],[1 4]),...
                                    0.95, color);
                end
                            
                if plot_meas_true_link
                    % connect with true position
                    connect_x = [x(k); z{s,l}(k).true(1)];
                    connect_y = [y(k); z{s,l}(k).true(4)];
                    plot(connect_x, connect_y,...
                        'Color', true_track_color(l,:),...
                        'LineStyle', '--',...
                        'LineWidth', 1,...
                        'Marker', 'none');
                end
            end
            
            measure_plots(s,l) = plot(x, y,...
                                'Color', color,...
                                'LineStyle', 'none',...
                                'Marker', 'x',...
                                'MarkerSize', 15);
        end
    end
    
    % --------------------------- associations ----------------------------
    if plot_assoc_triangles
        for k = 1:length(assoc)
            for f = 1:length(assoc(k).fusions)
                % won't work in sensor > 2 case
                % skip if wasn't update
                %{
                if any(assoc(k).fusions(f).fresh_info)
                    continue
                end
                %}
                % plot triangle
                id1 = assoc(k).ids{1}(assoc(k).fusions(f).assignment_tracks(1) - 1);
                id2 = assoc(k).ids{2}(assoc(k).fusions(f).assignment_tracks(2) - 1);

                % assoc(k).fusions(f).state(1)
                % assoc(k).fusions(f).state(4)

                % flat(id1).state(1)
                % flat(id1).state(4)

                % flat(id2).state(1)
                % flat(id2).state(4)

                x = [assoc(k).fusions(f).state(1); flat(id1).state(1); flat(id2).state(1); assoc(k).fusions(f).state(1)];
                y = [assoc(k).fusions(f).state(4); flat(id1).state(4); flat(id2).state(4); assoc(k).fusions(f).state(4)];

                plot(x, y, '-k');
            end        
        end
    end
    
    % ----------------------------- fusions -------------------------------
    fusion_color(1,1,:) = [0.5 0.5 1];
    fusion_color(1,2,:) = [0.5 0 1];
    fusion_color(2,1,:) = [1 0 0.5];
    fusion_color(2,2,:) = [1 0.5 0.5];
        
    fusion_plots = zeros(2);
    
    for l1 = 1:tracks_count
        for l2 = 1:tracks_count
            x = [];
            y = [];

            for k = 1:length(assoc)
                fus = [];
                for f = 1:length(assoc(k).fusions)
                    % assoc(k).fusions(f).tracks == l*ones([1 sensors_count])
                    if all(assoc(k).fusions(f).tracks == [l1, l2])
                       fus = assoc(k).fusions(f);
                       break
                    end
                end

                if isempty(fus)
                    continue
                end

                x = cat(1, x, fus.state(1)); % extract x coordinate
                y = cat(1, y, fus.state(4)); % extract y coordinate

                if plot_fus_cov
                    error_ellipse(  fus.state(1), fus.state(4),...
                                    fus.cov([1 4],[1 4]),...
                                    0.95, fusion_color(l1,l2,:));
                end
            end

            if isempty(x)
                continue
            end
            
            fusion_plots(l1, l2) = plot(x, y,...
                                        'Color', fusion_color(l1,l2,:),...
                                        'Marker', 'x');
        end
    end
    
    % ------------------------------ legend -------------------------------
    labels = cell(0);
    
    for i = 1:length(track_plots)
        labels = cat(2, labels, sprintf('track %d', i));
    end
    
    for i = 1:length(measure_plots(:))
        [s, l] = ind2sub([sensors_count tracks_count], i);
        labels = cat(2, labels, sprintf('sensor%d/track%d', s, l));
    end
    
    for i = 1:length(fusion_plots(:))
        if fusion_plots(i) == 0
            continue
        end
        
        [l1, l2] = ind2sub([tracks_count tracks_count], i);
        labels = cat(2, labels, sprintf('fusion %d+%d', l1, l2));
    end
    
    fusion_plots(fusion_plots == 0) = [];
    
    legend([track_plots measure_plots(:)' fusion_plots(:)'], labels)
    
    % ------------------------------ slider -------------------------------
    %{
    % slider
    pos = get(gca, 'position');
    newpos = [pos(1) pos(2)-0.1 pos(3) 0.05];
    slider = uicontrol('style', 'slider');
    slider.Units = 'normalized';
    slider.Position = newpos;
    % slider.Callback = @slider_callback;
    
    function slider_callback(src, ~)
        val = round(get(src,'value') / 0.01 + 1);
        
        % hide prev, show current association
    end
    
    %}
    
    % ------------------------------- etc ---------------------------------
    daspect([1 1 1]);
    % xlim([]);
    % ylim([]);
    
    hold off
end