classdef Sensor < handle
    properties
        error; % measurement covariation for each component
        polar_error; % measurement covariation for distance, azimuth
        dt; % seconds
        pos; % meters
        azimuth_start; % radian
        azimuth_end; % radian
        azimuth_step; % radian
        current_state; % [current_azimuth, current_direction]
        % current_direction == true; for left-to-right, false otherwise
        miss; % probability
        color; % plot: track measurements colors
    end
    
    methods
        function init_state(this)
            this.current_state = [this.azimuth_start, true];
        end
        
        function out = distance(this, state)
            x_pos(1) = state(1) - this.pos(1);
            x_pos(2) = state(4) - this.pos(2);
            
            out = norm(x_pos);
        end
        
        % in radian, relative to sensor pos
        function out = azimuth(this, state)
            x_pos(1) = state(1) - this.pos(1);
            x_pos(2) = state(4) - this.pos(2);

            azimuth = atan(x_pos(2) / x_pos(1));

            if x_pos(1) <= 0 % II, III quadrant
                azimuth = azimuth + pi;
            elseif x_pos(1) > 0 & x_pos(2) < 0 % IV quadrant
                azimuth = azimuth + 2*pi;
            end

            out = azimuth;
        end
        
        function out = is_it_see(this, x_true)
            if rand < this.miss
                % we already missed the target
                out = false; 
                return
            end

            x_true_az = this.azimuth(x_true);
            
            az_curr = this.current_state(1);
            curr_direction = this.current_state(2);
            
            % if target in field of view, we can try measure it
            if curr_direction
                out = x_true_az >= az_curr & x_true_az <= az_curr + this.azimuth_step;
            else
                out = x_true_az <= az_curr & x_true_az >= az_curr - this.azimuth_step;
            end
        end
        
        function next_state(this)
            az_curr = this.current_state(1);
            curr_direction = this.current_state(2);

            % shift to -pi;pi
            if az_curr > pi
                az_curr = 2*pi - az_curr;
            end
            
            az_start = this.azimuth_start;
            if az_start > pi
                az_start = 2*pi - az_start;
            end
            
            az_end = this.azimuth_end;
            if az_end > pi
                az_end = 2*pi - az_end;
            end
            
            % state in illegal angle
            if      az_curr < az_start + 0 ...
                  | az_curr > az_end - 0
              
                curr_direction = ~curr_direction;
            end

            if curr_direction
                az_curr = az_curr + this.azimuth_step;
            else
                az_curr = az_curr - this.azimuth_step;
            end

            if az_curr > 2*pi
                az_curr = az_curr - 2*pi;
            elseif az_curr < 0
                az_curr = az_curr + 2*pi;
            end

            next_state = [az_curr, curr_direction];
            this.current_state = next_state;
        end
        
        function out = get_delay(this, x_true)            
            out = 2*this.distance(x_true) / 3e8; % lightspeed
        end
    end
end