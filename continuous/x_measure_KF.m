function est = x_measure_KF(sensor, x0_true, max_time, x0_noise)
    est = [];
    sensor.init_state();
    
    % x, y position selection
    H = [1 0 0 0 0 0; 0 0 0 1 0 0];
    % process noise cov
    Q = 0;    
    noise_model = false;
    if length(x0_true) > 1
        noise_model = true;
    end
    
    i = 1; % measurement counter
    for current_time = 0:sensor.dt:max_time
        x_true = [];
        
        if noise_model
            x_true = get_x_true(x0_true, current_time)';
        else
            % vector-column
            x_true = state_transition(current_time - x0_true.time) * x0_true.state';
        end
        
        if sensor.is_it_see(x_true)
            distance = normrnd(sensor.distance(x_true), sensor.polar_error(1));
            azimuth = normrnd(sensor.azimuth(x_true), sensor.polar_error(2));
            
            % rotation = [cos(az), -ro*sin(az); sin(az), ro*cos(az)];
            rotation = [cos(azimuth), -distance*sin(azimuth); 
                       sin(azimuth),  distance*cos(azimuth)];
            % current measurement cov
            R = rotation * diag(sensor.polar_error.^2) * rotation';
            % R = H * sensor.error * H';
            
            % fprintf("dist/az: %5.0f, %3.1f\n", distance, azimuth);
            
            if i <= 3
                est(i).time = current_time + sensor.get_delay(x_true);
                est(i).true = x_true';
                est(i).cov = sensor.error + (4-i)*H'*R*H;
                est(i).state = mvnrnd(x_true, est(i).cov);
                
                i = i + 1;
                continue
            end
            
            %{
            % init
            if i == 1 | i == 2 | i == 3      
                est(i).time = current_time + sensor.get_delay(x_true);
                est(i).true = x_true';
                
                % replace state cov with measurment cov for init purposess
                est(i).cov = R;

                est(i).state = zeros([1 6]);

                % position
                est(i).state(1) = normrnd(x_true(1), sensor.error(1,1)); % distance*sin(azimuth);
                est(i).state(4) = normrnd(x_true(4), sensor.error(4,4)); % distance*cos(azimuth);

                % velocity
                if i == 2 | i == 3
                    dt = est(i).time - est(i-1).time;
                    
                    est(i).state(2) = (est(i).state(1) - est(i-1).state(1)) / dt;
                    est(i).state(5) = (est(i).state(4) - est(i-1).state(4)) / dt;

                    % acceleration
                    if i == 3
                        est(i).state(3) = (est(i).state(2) - est(i-1).state(2)) / dt;
                        est(i).state(6) = (est(i).state(5) - est(i-1).state(5)) / dt;  

                        % prev measurement cov, not state cov
                        prev_cov = est(i-1).cov;
                        prev_prev_cov = est(i-2).cov;

                        % compose cov from 3x3 blocks (3 cuz state has pos, vel, acc)
                        for m = 1:2 % coordinate
                            for n = 1:2 % coordinate
                                P = zeros(3);
                                P(1,:) = [R(m,n), R(m,n)/dt, R(m,n)/dt^2];

                                % P(2,1) = R(m,n)/dt;
                                P(2,1) = P(1,2);
                                P(2,2) = (R(m,n) + prev_cov(m,n)) / dt^2;
                                P(2,3) = (R(m,n) + 2*prev_cov(m,n)) / dt^3;

                                % P(3,1) = R(m,n)/dt^2;
                                % P(3,2) = (R(m,n) + 2*prev_cov(m,n)) / dt^3;
                                P(3,1) = P(1,3);
                                P(3,2) = P(2,3);
                                P(3,3) = (R(m,n) + 4*prev_cov(m,n) + prev_prev_cov(m,n)) / dt^4;

                                % state cov
                                est(i).cov(3*m-2:3*m, 3*n-2:3*n) = P;
                            end
                        end
                    end
                end

                i = i + 1;
                continue;
            end
            %}
            
            dt = current_time - est(i-1).time;
            F = state_transition(dt);
            
            if noise_model
                % noise to state transition
                G = [dt^2/2 0; dt 0; 0 0;
                     0 dt^2/2; 0 dt; 0 0]; % G*v = process noise
                
                Q = G*x0_noise*G';
            end
            
            cov_predict = F * est(i-1).cov * F' + Q;
            
            S = H * cov_predict * H' + R; % innovation cov
            K = cov_predict * H' / S; % gain
            
            x_predict = F * est(i-1).state';
            z_predict = H * x_predict;
            z_true = [distance*cos(azimuth); distance*sin(azimuth)] + sensor.pos';
            
            % vector-row
            est(i).state = (x_predict + K * (z_true - z_predict))';
            est(i).cov = cov_predict - K * S * K';
            est(i).time = current_time + sensor.get_delay(x_true);
            est(i).true = x_true';
            
            % fprintf("%d\n", issymmetric(est(i).cov));

            i = i + 1;
        end
        
        sensor.next_state();        
    end
    
    % velocity and acceleration is unknown at this steps, omit it
    est(1:3) = [];
end