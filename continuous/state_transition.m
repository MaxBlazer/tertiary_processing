function F = state_transition(t)
    F(1:3,1:3) = [1 t t^2/2; 0 1 t; 0 0 1];
    F(4:6,4:6) = F(1:3,1:3);
end