addpath("..");

% initial data
config 
% x0 = struct(state, time)
% sensor = struct(...)
% max_time
% tracks_count = length(x0);
% sensors_count = length(sensor);

rng(0);

x = cell([1 tracks_count]);
for i = 1:tracks_count
    x{i} = x_true_process_noise(x0(i), max_time);
end

z = cell(tracks_count, tracks_count);
for s = 1:sensors_count
    for l = 1:tracks_count
        % z{s,l} = x_measure(sensor(s), x0(l), max_time);
        % z{s,l} = x_measure(sensor(s), x{l}, max_time);
        z{s,l} = x_measure_KF(sensor(s), x0(l), max_time);
        % z{s,l} = x_measure_KF(sensor(s), x{l}, max_time, x0(l).noise);
    end
end

clear s
clear l

flat = flatten_measures(z);
% assoc = association(flat, sensor);
assoc_wo_cross = association_wo_cross(flat, sensor);
plot_all(x0, z, max_time, sensor, assoc_wo_cross, flat);
print_accuracy(assoc_wo_cross);
