dT = 1;

man_index_range = [0.01:0.005:0.1 0.1:0.05:1 1:0.5:5];

scale = 1; % for testing

G = [dT^2/2; dT]; % G*v = process noise
F = [1 dT; 0 1];
H = [1 0];
nx = 2; % = length(state)
I = eye(nx); % eye(size(K*H));

% row for each coordinate, cols for man index
ros = cell([1 length(man_index_range)]);

for i = 1:length(man_index_range)
    q = (scale*man_index_range(i) / dT^2)^2; % cov(v)
    Q = G*q*G'; % process noise cov; cov(G*v)

    R{1} = scale^2; % meas cov for 1 sensor
    R{2} = 1/5 .* R{1};  % meas cov for 2 sensor

    cross = cell(1);
    P = cell([2 1]);

    cross{1} = zeros(2);
    P{1,1} = diag([100 20] .^ 2); % initial cov
    P{2,1} = diag([100 20] .^ 2); % initial cov

    max_k = 1000;

    K = cell([1 2]); % filter gains
    for k = 2:max_k
        for s = 1:2 % each sensor
            P_predict = F*P{s, k-1}*F' + Q;
            S = H * P_predict * H' + R{s}; % z - z_predict cov
            K{s} = P_predict * H'/S; % filter gain
            P{s,k} = P_predict - K{s}*S*K{s}';
        end
        
        cross{k} = (I - K{1}*H)*(F * cross{k-1} * F' + Q)*(I - K{2}*H)';
    end

    ro = cross{end} ./ sqrt(diag(P{1, end}) * diag(P{2, end})');
    ro(isnan(ro)) = 0;

    ros{i} = ro;
end

rx = zeros([length(man_index_range) 4]);

average = zeros([1 4]);

for i = 1:length(man_index_range)
    rx(i,:) = flip([ros{1,i}(1,1), ros{1,i}(1,2), ros{1,i}(2,1), ros{1,i}(2,2)], 2);    
    
    average(:) = average(:) + rx(i,:)';    
end

average(:) = average(:) / length(man_index_range);
% average

figure('Name', 'Approximation coefficients');

plot(man_index_range, rx);
title('x component');
set(gca, 'Xscale', 'log');
legend(flip(["ρ11", "ρ12", "ρ21", "ρ22"], 2));

