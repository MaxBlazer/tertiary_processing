function out = likelihood_matrix_w_cross(z, sensor)
    global mu
    sensors_count = length(z);
    
    if sensors_count < 2
        error("Outer dimension of x (%d) is less than two.\nTo get likelihood matrix, 2 or more sensors needed.", sensors_count);
        return
    end
    
    if sensors_count > 2
        error("More than 2 sensors is not developed yet.");
        return
    end

    tracks_count = [];
    for s = 1:sensors_count
        tracks_count = cat(2, tracks_count, length(z{s}));
    end
    out = zeros(tracks_count);
    
    % mu = 9.1989e-21;
    
    % only for 2 sensors
    for l1 = 1:tracks_count(1)
        for l2 = 1:tracks_count(2)
            zz = cat(2, z{1}(l1), z{2}(l2));            
            
            % if dummy, sensors_count < 2!
            koef = mu^(1-sensors_count) * (1-sensor(1).miss)*(1-sensor(2).miss);
            out(l1, l2) = koef * likelihood_elem_w_cross(zz);
        end
    end
end