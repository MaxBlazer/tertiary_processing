function out = association(z, sensor)
    sensors_count = length(sensor);

    estimation = cell(sensors_count, 1);
    estimation_ids = cell(sensors_count, 1); % for plotting purposes
    known = cell(sensors_count, 1);
    fusion_indicies = cell(sensors_count, 1);
    fusion = [];
    
    threshold = chi2inv(0.97, length(z(1).state));
    
    % estimation
    % { [state2 state1], [state3], [state4 state5] }
    
    % known - known track indicies (sensor-dependent)
    % {[2 1], [3 4 1], [5]}
    
    % fusion.tracks = [s1_track s2_track ...]
    % fusion.state
    % fusion.cov
    % fusion.approved = false
    % fusion.fails = 3
    % fusion.success = [3 3 ...]
    
    fresh_info = false(1, sensors_count);
    out = [];
    
    for k = 1:length(z)       
        s = z(k).sensor;
        l = z(k).track;
        
        fresh_info(s) = true;
        
        track_index = find(known{s} == l);
        
        % if z(k).sensor/track index is known
        if track_index
            % refresh old
            estimation{s}(track_index) = z(k);
            estimation_ids{s}(track_index) = k;
            
            % try to refresh corresponding fusion if exist
            i = fusion_indicies{s}(track_index);
            if i                
                % extrapolation
                F = state_transition(z(k).time - fusion(i).time);
                fusion(i).state = fusion(i).state * F';
                fusion(i).cov = F * fusion(i).cov * F';
                fusion(i).time = z(k).time;
                
                % normalized distance squared
                % global ro
                % cross = ro .* sqrt(diag(z(k).cov) * diag(fusion(i).cov)');
                cross = 0;
                delta = fusion(i).state - z(k).state;
                delta_cov = fusion(i).cov + z(k).cov - (cross + cross');

                dist = delta * inv(delta_cov) * delta';
                belong_to = dist < threshold;

                if belong_to
                    % делаем вид что ничего не было
                    fresh_info(s) = false;
                    fusion(i) = fuse_w_cross(fusion(i), z(k));

                    if fusion(i).approved
                        fusion(i).fails = 0;
                    else
                        fusion(i).success(s) = fusion(i).success(s) + 1;

                        if fusion(i).success >= 3*ones([1 sensors_count])
                            fusion(i).approved = true;
                            % fusion(i) = rmfield(fusion(i), 'success');
                        end
                    end
                else
                    if fusion(i).approved
                        fusion(i).fails = fusion(i).fails + 1;

                        if fusion(i).fails >= 3
                            fusion(i) = [];

                            % refresh fusion indicies
                            fusion_indicies = delete_fusion(i, fusion_indicies);
                        else 
                            fresh_info(s) = false;
                        end
                    else
                        fusion(i) = [];

                        % refresh fusion indicies
                        fusion_indicies = delete_fusion(i, fusion_indicies);
                    end
                end
            end
        else
            % add new
            known{s} = cat(2, known{s}, l);
            last_relative_index = length(known{s}); % index in assignment matrix
            
            estimation{s}(last_relative_index) = z(k);
            estimation_ids{s}(last_relative_index) = k;
            fusion_indicies{s}(last_relative_index) = 0; % no fusion
        end
        
        % declaration
        assignment = []; 
        lambda = [];
        if fresh_info
            fresh_info = false(1, sensors_count);
            
            % extrapolate estimations
            for s = 1:sensors_count
                for l = 1:length(estimation{s})
                    % skip estimates in fusions
                    if fusion_indicies{s}(l)
                        continue;
                    end
                    
                    F = state_transition(z(k).time - estimation{s}(l).time);
                    estimation{s}(l).state = estimation{s}(l).state * F';
                    estimation{s}(l).true = estimation{s}(l).true * F';
                    estimation{s}(l).time = z(k).time;
                end
            end
            
            estimation_wo_fusions = estimation;
            
            for s = 1:sensors_count
                for l = length(estimation{s}):-1:1
                   if fusion_indicies{s}(l)
                       estimation_wo_fusions{s}(l) = [];
                   end
                end
            end
            
            lambda = likelihood_matrix_w_cross(estimation_wo_fusions, sensor);            
            cost = log(lambda);
            
            % replace -inf elements with doubled minimum value
            inf_elems = find(cost == -inf);
            if inf_elems
                cost_copy = cost;
                cost_copy(inf_elems) = [];
                               
                if length(cost_copy(:)) < 2
                    minimum = -100;
                else 
                    minimum = -2*abs(min(cost_copy(:)));
                end
                
                cost(inf_elems) = minimum;
            end
            
            % add dummy elements to cost (2 sensor case)
            cost(2:end+1, 2:end+1) = cost;
            cost(1,:) = 0;
            cost(:,1) = 0;
            
            assignment = auction(cost);
        end      
        
        if isempty(assignment) 
            % there wasn't association at this step
            % but was fusion update
            
            if isempty(out)
                continue
            end
            
            % copy old data
            result = out(end);
            
            result.time = z(k).time;
            result.tracks = known;
            result.ids = estimation_ids;
            
            % refresh fusions
            result.fusions = fusion;

            out = cat(2, out, result); 
            continue
        end
        
        % add fusions tracks to assignment matrix
        for i = 1:length(fusion)
            % 2 sensor case
            l1 = fusion(i).assignment_tracks(1);
            l2 = fusion(i).assignment_tracks(2);
            
            expanded = false(size(assignment) + 1);
            
            expanded([1:l1-1 l1+1:end], [1:l2-1 l2+1:end]) = assignment;
            assignment = expanded;
        end
        
        assignment_wo_dummies = assignment(2:end, 2:end); % 2 sensor case
        
        [l1, l2] = find(assignment_wo_dummies);        
        
        for i = 1:length(l1)            
            % create fusion (hypothesis)
            new_fusion.state = estimation{1}(l1(i)).state;
            new_fusion.cov = estimation{1}(l1(i)).cov;
            new_fusion.time = estimation{1}(l1(i)).time; % = estimation{2}(l2(i)).time
            new_fusion = fuse_w_cross(new_fusion, estimation{2}(l2(i)));
            
            % actual track indicies
            new_fusion.assignment_tracks = [l1(i)+1, l2(i)+1]; % dummy shift 
            new_fusion.tracks = [known{1}(l1(i)), known{2}(l2(i))];
            new_fusion.approved = false; % = hypothesis
            new_fusion.success = zeros([1 sensors_count]);
            new_fusion.fails = 0;
            
            fusion = cat(2, fusion, new_fusion);
            
            new_index = length(fusion);
            % don't consider this tracks in following associations
            fusion_indicies{1}(l1(i)) = new_index;
            fusion_indicies{2}(l2(i)) = new_index;
        end
        
        % add fusions associations to assignment matrix
        for i = 1:length(fusion)
            % 2 sensor case
            l1 = fusion(i).assignment_tracks(1);
            l2 = fusion(i).assignment_tracks(2);
            
            assignment(l1, l2) = true;
        end
        
        result.data = assignment; 
        result.lambda = lambda; 
        result.time = z(k).time;
        result.true = true_association(estimation);
        result.success = result.data == result.true;
        result.tracks = known;
        result.ids = estimation_ids;
        result.fusions = fusion;

        out = cat(2, out, result);        
    end
end

function out = delete_fusion(i, fusion_indicies)
    for s = 1:length(fusion_indicies)
        for l = 1:length(fusion_indicies{s})
            if fusion_indicies{s}(l) == i
                fusion_indicies{s}(l) = 0;
            elseif fusion_indicies{s}(l) > i
                fusion_indicies{s}(l) = fusion_indicies{s}(l) - 1;
            end
        end
    end
    
    out = fusion_indicies;
end