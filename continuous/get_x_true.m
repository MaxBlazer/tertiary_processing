function out = get_x_true(x_true, time)
    min_time = x_true(1).time;
    max_time = x_true(end).time;
    
    % find non integer index in x_true array
    index = (time - min_time) / (max_time - min_time) * (length(x_true) - 1) + 1;
    index1 = floor(index);
    index2 = index1 + 1; % ceil(index);
    k2 = index - index1;
    k1 = 1 - k2;
    
    out = k1*x_true(index1).state + k2*x_true(index2).state;
end