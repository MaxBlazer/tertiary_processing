function out = true_association(z)
    sensors_count = length(z);
    
    tracks_count = [];
    for s = 1:sensors_count
        tracks_count = cat(2, tracks_count, length(z{s}));
    end
    out = false(tracks_count);
    
    for l1 = 1:tracks_count(1)
        for l2 = 1:tracks_count(2)            
            out(l1, l2) = z{1}(l1).track == z{2}(l2).track;
        end
    end
    
    out(2:end+1, 2:end+1) = out;
    out(1,:) = 0;
    out(:,1) = 0;
    
    return % !
    
    % only for 2 sensors
    for l1 = 1:tracks_count(1)
        for l2 = 1:tracks_count(2)            
            out(l1, l2) = norm(z{1}(l1).true - z{2}(l2).true);
        end
    end
    
    transposed = false;
    if tracks_count(1) > tracks_count(2)
        transposed = true;
        out = out';
    end
    
    out = -log(out);
    
    out(out > 0) = 1;
    out(out < 0) = 0;
   
    out(2:end+1, 2:end+1) = out;
    out(1,:) = 0;
    out(:,1) = 0;
    
    out = logical(out);
   
    for i = 2:size(out, 2)
        if ~out(:,i)
            out(1,i) = true;
        end
    end
    
    if transposed
        out = out';
    end
    
    % out = auction(out);
end