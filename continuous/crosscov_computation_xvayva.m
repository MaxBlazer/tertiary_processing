dT = 1;

man_index_range = [0.01:0.005:0.1 0.1:0.05:1 1:0.5:5];

scale = 1; % for testing

G = [dT^2/2 0; dT 0; 0 0;
     0 dT^2/2; 0 dT; 0 0]; % G*v = process noise
F = state_transition(dT);
H = [1 0 0 0 0 0; 0 0 0 1 0 0];
nx = 6; % = length(state)
I = eye(nx); % eye(size(K*H));

% row for each coordinate, cols for man index
ros = cell([2 length(man_index_range)]);

for coordinate = 1:2
    for i = 1:length(man_index_range)
        maneuvering_index = []; %#ok<NASGU>
        
        if coordinate == 1
            maneuvering_index = [man_index_range(i) 1];
        else
            maneuvering_index = [1 man_index_range(i)];
        end
        
        q = diag((scale*maneuvering_index / dT^2) .^ 2); % cov(v)
        Q = G*q*G'; % process noise cov; cov(G*v)
        % Q = zeros(size(Q));
        R = diag((scale*[1 1]) .^ 2); % meas cov, equals for both sensors

        cross = cell(1);
        P = cell(1);

        cross{1} = zeros(6);
        P{1} = diag([100 20 20 100 20 20] .^ 2); % initial state doesn't matter

        max_k = 1000;

        for k = 2:max_k
            P_predict = F*P{k-1}*F' + Q;
            S = H * P_predict * H' + R; % z - z_predict cov
            K = P_predict * H'/S; % filter gain
            P{k} = P_predict - K*S*K';

            D = I - K*H;
            cross{k} = D*(F * cross{k-1} * F' + Q)*D';
        end

        % todo: unequal measurement noises
        ro = cross{end} ./ P{end};
        ro(isnan(ro)) = 0;

        ros{coordinate, i} = ro;
    end
end

rx = zeros([length(man_index_range) 6]); % 6 = count of meaning ρ component
ry = zeros([length(man_index_range) 6]);

average = zeros([2 6]);

for i = 1:length(man_index_range)
    rx(i,:) = flip([ros{1,i}(1,1), ros{1,i}(1,2), ros{1,i}(1,3), ros{1,i}(2,2), ros{1,i}(2,3), ros{1,i}(3,3)], 2);    
    ry(i,:) = flip([ros{2,i}(4,4), ros{2,i}(4,5), ros{2,i}(4,6), ros{2,i}(5,5), ros{2,i}(5,6), ros{2,i}(6,6)], 2);
    average(1,:) = average(1,:) + rx(i,:);
    average(2,:) = average(2,:) + ry(i,:);
end

average(1,:) = average(1,:) / length(man_index_range);
average(2,:) = average(2,:) / length(man_index_range);
average

figure('Name', 'Approximation coefficients');

top_plot_ax = subplot(1, 2, 1);
plot(man_index_range, rx);
title('x component');
set(top_plot_ax, 'Xscale', 'log');
legend(flip(["ρ11", "ρ12", "ρ13", "ρ22", "ρ23", "ρ33"], 2));

bot_plot_ax = subplot(1, 2, 2);
plot(man_index_range, ry);
title('y component');
set(bot_plot_ax, 'Xscale', 'log');
legend(flip(["ρ44", "ρ45", "ρ45", "ρ55", "ρ56", "ρ66"], 2));
