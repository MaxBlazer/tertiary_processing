% 2 sensor case
function out = fuse_w_cross(receiver, x)    
    X = [x.state, receiver.state]';
    
    global ro
    P_cross = ro .* sqrt(diag(x.cov) * diag(receiver.cov)');
    
    % костыль, игнорируем недиагональные элементы кроссковариации
    for i = 1:size(P_cross, 1)
        for j = 1:size(P_cross, 2)
            if i ~= j & P_cross(i,j) < 0
                P_cross(i,j) = 0;
            end
        end
    end
    
    P_cross = zeros(6);    
    
    P = [x.cov,     P_cross;
         P_cross',  receiver.cov];
    
    P_inv = inv(P);

    x_len = length(x.state);
    I = [eye(x_len); eye(x_len)];
    
    receiver.cov = inv(I' * P_inv * I);
    receiver.state = (receiver.cov * I' * P_inv * X)';

    out = receiver;
end