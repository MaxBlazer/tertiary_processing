% 2 sensor case
function out = fuse_wo_cross(receiver, x)
    X = [x.state, receiver.state]';

    % no crosscovariation
    P = [x.cov,     zeros(6);
         zeros(6),  receiver.cov];
    
    P_inv = inv(P);

    x_len = length(x.state);
    I = [eye(x_len); eye(x_len)];
    
    receiver.cov = inv(I' * P_inv * I);
    receiver.state = (receiver.cov * I' * P_inv * X)';

    out = receiver;
end