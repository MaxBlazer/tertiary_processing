dT = 1;

man_index_range = [0.01:0.005:0.1 0.1:0.05:1 1:0.5:5];

scale = 1; % for testing

G = [dT^2/2; dT]; % G*v = process noise
F = [1 dT; 0 1];
H = [1 0];
nx = 2; % = length(state)
I = eye(nx); % eye(size(K*H));

% row for each coordinate, cols for man index
ros = cell([1 length(man_index_range)]);

for i = 1:length(man_index_range)
    q = (scale*man_index_range(i) / dT^2)^2; % cov(v)
    Q = G*q*G'; % process noise cov; cov(G*v)
    R = scale^2; % meas cov for each sensor

    cross = cell(1);
    P = cell(1);

    cross{1} = zeros(nx);
    P{1} = diag([100 20] .^ 2); % initial state doesn't matter

    max_k = 1000;

    for k = 2:max_k
        P_predict = F*P{k-1}*F' + Q;
        S = H * P_predict * H' + R; % z - z_predict cov
        K = P_predict * H'/S; % filter gain
        P{k} = P_predict - K*S*K';

        D = I - K*H;
        cross{k} = D*(F * cross{k-1} * F' + Q)*D';
    end

    P_fusion_w_cross = (P{end} + cross{end}) / 2;
    % ro = 2 * (P_fusion_w_cross ./ P{end}).^(2/nx) - 1;
    ro = cross{end} ./ P{end};
    % ro(isnan(ro)) = 0;

    % ros{i} = ro;
    ros{i} = P_fusion_w_cross ./ P{end}; % for plot as in book
end

rx = zeros([length(man_index_range) 3]);

average = zeros([1 3]);

for i = 1:length(man_index_range)
    rx(i,:) = flip([ros{i}(1,1), ros{i}(1,2), ros{i}(2,2)], 2);    
    average = average + rx(i,:);
end

average = average / length(man_index_range);
average

plot(man_index_range, rx);
title('x component');
set(gca, 'Xscale', 'log');
legend(flip(["ρ11", "ρ12", "ρ22"], 2));

