% model: constant velocity + white noise acceleration

function out = x_true_process_noise(x0, max_time)
    dt = 0.1;
    F = state_transition(dt);
    G = [dt^2/2 0; dt 0; 0 0];
    G(4:6, 2) = G(:,1);

    % todo: fix time
    x.state = x0.state;
    x.time = x0.time;
    out = x;
    i = 2;
    
    for time = dt:dt:max_time
       x.time = time;
       x.state = (F*out(i-1).state' + G*mvnrnd([0 0], x0.noise)')';
       out(i) = x;
       i = i + 1;
    end
end