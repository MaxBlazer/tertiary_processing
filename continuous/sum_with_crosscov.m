function out = sum_with_crosscov(cov1, cov2)
    global ro
    cross = ro .* sqrt(diag(cov1) * diag(cov2)');

    out = cov1 + cov2 -(cross + cross');
    
    % костыль, игнорируем недиагональные элементы кроссковариации
    for i = 1:size(out, 1)
        for j = 1:size(out, 2)
            if i ~= j & out(i,j) < 0
                out(i,j) = 0;
            end
        end
    end
end