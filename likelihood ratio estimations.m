% x(l,i): l -- sensor/track, i -- state component
% P(i,j,l): l -- sensor/track, i*j -- covariation
x(1,:) = [0 0 0 0 0 0];
x(2,:) = 1.39*ones([1 6]); % k*[1 1 1 1 1 1] 

sigma = 1.5; % 1.8556
% sigma = (4*pi)^(-1/2)*nthroot(P_D*P_D/mu, 6);
P(:,:,1) = diag(sigma^2*ones([1 6]));
P(:,:,2) = P(:,:,1);

P_delta = 2*P(:,:,1);

% coefficients
mu = 1.9e-9;
P_D = 0.9;
sensors_count = 2;

% mu^(1-sensors_count) * (P_D)^sensors_count * likelihood_elem(x, P)
mu^(1-sensors_count) * (P_D)^sensors_count * mvnpdf(x(2,:), x(1,:), P_delta)
% mu^(1-sensors_count) * (P_D)^sensors_count * mvnpdf(zeros([1 6]), 0, P_delta)

% sigma <= 3.5 --> mu >= P_D^2 * (4*pi)^(-6/2) * 3.5^(-6)
% P_D^2 * (4*pi)^(-9/2) * 50^(-9)

ss = 0:0.1:10;
dx = sqrt(-4/6*ss.^2 .* log((4*pi*ss.^2).^(6/2) * mu*P_D^(-2)));

plot(ss, dx);

% -4*ss.^2 .* log((4*pi*ss.^2).^(6/2) * mu*P_D^(-2))

% max_dist = nthroot(4.7e23 / (deg2rad(15)/3), 3)
% = 1.75e8