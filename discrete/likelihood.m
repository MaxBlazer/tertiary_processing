% x{s}(l,i): s -- sensor, l -- track, i -- state component
% P{s}(i,j,l): s -- sensor, l -- track, i*j -- covariance
function out = likelihood(x, P)
    sensors_count = length(x);
    
    if sensors_count < 2
        error("Outer dimension of x (%d) is less than two.\nTo get likelihood matrix, 2 or more sensors needed.", sensors_count);
        return
    end
    
    if sensors_count > 2
        error("More than 2 sensors is not developed yet.");
        return
    end

    out_size = [];
    for s = 1:sensors_count
        out_size = cat(2, out_size, size(x{s}, 1));
    end
    
    out = zeros(out_size);

    % coefficients
    Nex = 100; % tracks
    r_max = 3.5e5; % m
    az_max = 90; % deg
    % elev_max = 15; % deg
    % V = r_max^3/3 * deg2rad(elev_max) * sin(deg2rad(az_max)); % = 3.7e15 m3
    V = r_max^2/2 * sin(deg2rad(az_max)); % 2d case
    mu = Nex / V;
    P_D = 0.9; % probability of detection
    
    % only for 2 sensors
    for l1 = 1:out_size(1)
        for l2 = 1:out_size(2)
            xx = cat(1, x{1}(l1,:), x{2}(l2,:));
            PP = cat(3, P{1}(:,:,l1), P{2}(:,:,l2));
            
            % likelihood_elem(xx, PP)
            
            % if dummy, sensors_count < 2!
            out(l1, l2) = mu^(1-sensors_count) * (P_D)^sensors_count * likelihood_elem(xx, PP);
        end
    end
end