function [x_meas, P_meas] = x_measure(x_true, P)    
    x_len = size(x_true, 2);
    track_len = size(x_true, 1);
    
    x_meas = zeros(track_len, x_len); % second dimention is single data
    P_meas = zeros(x_len, x_len, track_len); % 1,2 dims are single data
       
    for i = 1:track_len          
        % covariation (just copy)
        P_meas(:,:,i) = P;
        % state
        x_meas(i,:) = mvnrnd(x_true(i,:), P_meas(:,:,i));
    end
end