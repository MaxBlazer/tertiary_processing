function plot_all_time(x, z, P, tracks_count, sensors_count, sensors_params, association_matrix)
    x_index = 1;
    y_index = 4;

    true_track_colors(1,:) = [0.5 1 0];
    true_track_colors(2,:) = [0 1 0.5];
    
    for l=1:tracks_count
        plot(x{l}(:,x_index), x{l}(:,y_index),...
             'Color', true_track_colors(l,:),...
             'LineStyle', '-',...
             'LineWidth', 2,...
             'Marker', 'x');
        hold on;

        for s=1:sensors_count
            color = sensors_params(s).color(l,:);
            
            plot(z{s,l}(:,x_index), z{s,l}(:,y_index),...
                 'Color', color,...
                 'LineStyle', 'none',...
                 'Marker', '.',...
                 'MarkerSize', 15);

            measures_count = length(z{s,l});

            for k=1:measures_count
                error_ellipse(  z{s,l}(k,x_index),...
                                z{s,l}(k,y_index),...
                                P{s,l}( [x_index y_index],...
                                        [x_index y_index], k ),...
                                0.95, color);
            end
        end
    end
    
    text(x{2}(:,x_index), x{2}(:,y_index),int2str((1:length(x{2}))'));
    
    misses = association_check(association_matrix);
    
    for i=1:length(misses)
        for j=1:tracks_count
            k = misses(i);
            first_track = find(association_matrix(:,j,k));

            plot(   [z{1,first_track}(k,x_index); z{2,j}(k,x_index)],... 
                    [z{1,first_track}(k,y_index); z{2,j}(k,y_index)],...
                    'Color', 'black');
            hold on;
        end
    end
    
    daspect([1 1 1]); % equal x/y scaling
    xlim([-650 -200]);
    ylim([-50 400]);
end