% x{s,l}(k,i): s -- sensor, l -- track, i -- state component, k -- time
% P{s,l}(i,j,k): s -- sensor, l -- track, k -- time, i*j -- covariation
function [out, lambda] = association(x, P, max_time)
    sensors_count = size(x, 1);
    tracks_count = size(x, 2); % !!! should be dynamic

    for k=1:max_time
        for s=1:sensors_count
            x_temp = [];
            P_temp = [];
            
            for l=1:tracks_count
                x_temp = cat(1, x_temp, x{s,l}(k,:)); 
                P_temp = cat(3, P_temp, P{s,l}(:,:,k));
            end
            
            xx{s} = x_temp;
            PP{s} = P_temp;
        end
        %{
        lambda(2:tracks_count+1, 2:tracks_count+1,k) = likelihood(xx, PP);
        lambda(1,:,k) = 1;
        lambda(:,1,k) = 1;
        %}
        
        lambda(:,:,k) = likelihood(xx, PP);
        % out(:,:,k) = auction(log(lambda(:,:,k)));
        out(:,:,k) = lapjv_fast(-log(lambda(:,:,k)));
    end
end