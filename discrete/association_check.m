function out = association_check(association)
    compare_to = eye(size(association(:,:,1)));
    % compare_to(1,1) = 0;
    
    max_time = size(association, 3);
    
    out = [];
    
    for k=1:max_time
        if (~isequal(association(:,:,k), compare_to))
            out = [out k];
        end
    end
end