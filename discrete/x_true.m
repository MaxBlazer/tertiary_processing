% x = (x vx ax y vy ay)
function out = x_true(x0, F, count)
    out = zeros(count, length(x0));
    out(1, :) = x0;    
    
    for i=2:count
        % x(k) = F*x(k-1)
        out(i,:) = out(i-1,:)*F';
    end
end