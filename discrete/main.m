addpath("..");

% ---------------------- True states generation ---------------------------
% x{l}(k,i): l -- track, i -- state component, k -- time

max_time = 20;
dT = 1;

F(1:3,1:3) = [1 dT dT^2/2; 0 1 dT; 0 0 1];
F(4:6,4:6) = F(1:3,1:3);

% parabolic tracks
% x{1} = x_true([100 -3 -0.5 200 -10 1], F, max_time);
% x{2} = x_true([50 7 -0.1 160 -2 0.5], F, max_time);
% x{2} = x_true([-50 7 -0.1 100 -2 0.5], F, max_time);
% x{3} = x_true([0 -2 0.2 120 7 -0.3], F, max_time);

% parallel tracks
% x{1} = x_true([-500 5 0 400 -20 0], F, max_time);
% x{2} = x_true([-480 5 0 400 -20 0], F, max_time);

% parallel close tracks
x{1} = x_true([-500 5 0 400 -20 0], F, max_time);
x{2} = x_true([-490 5 0 400 -20 0], F, max_time);

% intersected tracks
% x{1} = x_true([-500 5 0 400 -20 0], F, max_time);
% x{2} = x_true([-400 -5 0 400 -20 0], F, max_time);

% --------------------- Measurements generation ---------------------------
sensors_count = 2;

sensors_params(1).error = 3*eye(6);
sensors_params(1).color(1,:) = [1 0.7 0]; % track 1
sensors_params(1).color(2,:) = [0 0.8 0.9]; % track 2

sensors_params(2).error = 6*eye(6);
sensors_params(2).color(1,:) = [1 0.2 0];
sensors_params(2).color(2,:) = [0 0 1];

tracks_count = length(x);
z = cell(tracks_count);
P = cell(tracks_count);

% z{s,l}(k,i): s -- sensor, l -- track, i -- state component, k -- time
% P{s,l}(i,j,k): s -- sensor, l -- track, k -- time, i*j -- covariation
for s=1:sensors_count
    for l=1:tracks_count
        [z{s,l}, P{s,l}] = x_measure(x{l}, sensors_params(s).error);
    end
end

% --------------------------- Association ---------------------------------

[association_matrix, lambda] = association(z, P, max_time);

association_check(association_matrix)

% -------------------------- Visualization --------------------------------

plot_all_time(x, z, P, tracks_count, sensors_count, sensors_params, association_matrix);

%{
    plot_at_time(1);

    pos = get(gca,'position');
    newpos = [pos(1) pos(2)-0.1 pos(3) 0.05];
    slider = uicontrol('style','slider');
    slider.Units = 'normalized';
    slider.Position = newpos;
    slider.Callback = @callback;
%}

%{
    time = ;
    xl{1} = cat(1, z{1,1}(time,:), z{1,2}(time,:)); 
    xl{2} = cat(1, z{2,1}(time,:), z{2,2}(time,:)); 

    Pl{1} = cat(3, P{1,1}(:,:,time), P{1,2}(:,:,time));
    Pl{2} = cat(3, P{1,1}(:,:,time), P{1,2}(:,:,time));

    lambda_ = likelihood(xl, Pl);
    cost = -log(lambda_);
    lapjv_fast(cost);
%}

%{
function callback(src, ~)
    k = round(get(src,'value') / 0.01 + 1);
    % k
    plot_at_time(k);
end

function plot_at_time(k)
    if k > 18
        return
    end
    
    hold off;
    
    for ll=1:tracks_count
        plot(x{ll}(k+2,1), x{ll}(k+2,2), '-rx');
        hold on;

        for ss=1:sensors_count
            plot(z{ss,ll}(k,1), z{ss,ll}(k,2),...
                'Color', sensors_params(ss).color, 'LineStyle', 'none', 'Marker', 'x');
            
            error_ellipse(z{ss,ll}(k,1), z{ss,ll}(k,2),...
                P{ss,ll}(1:2,1:2,k), 0.95, sensors_params(ss).color);            
        end
    end    
    
    % association_matrix(:,:,k)
    
    daspect([1 1 1]);
    xlim([-100 200]);
    ylim([100 400]);
end

end
%}