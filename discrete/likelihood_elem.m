% x(l,i): l -- sensor/track, i -- state component
% P(i,j,l): l -- sensor/track, i*j -- covariation
function out = likelihood_elem(x, P)
    % amount of associated tracks
    tracks_count = size(x, 1); % = sensor amount
    % x_len = length(x(1,:));
    
    if tracks_count < 2
        error("First dimension of x (%d) is less than two.\nTo get likelihood element, 2 or more estimates needed.", tracks_count);
        return
    end
    
    % delta = [deltaN1 ... delta21]
    delta = [];
    
    for i=2:tracks_count
        delta = cat(2, delta, x(i,:) - x(1,:));
    end

    % only for S = 2!!!
    % (diagonal block only)
    
    % ro = [0.15 0.25; 0.25 0.7]; for [x vx] state
    
    ro = [  0.15 0.25 0.35;
            0.25 0.3  0.2; 
            0.35 0.2  0.5   ];
    
    ro(4:6,4:6) = ro;
    
    % cross = ro .* sqrt(diag(P(:,:,1)) * diag(P(:,:,2))');
    cross = 0;
    P_delta = P(:,:,1) + P(:,:,2) -(cross + cross');
    % ---
    
    out = mvnpdf(delta, 0, P_delta);
end