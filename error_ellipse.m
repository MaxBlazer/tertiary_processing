function error_ellipse(x0, y0, covariance, confidence, color, plot_axes)
    circle_sides = 12;

    [eigenvec, eigenval] = eig(covariance);
    
    % Get the index of the largest eigenvector
    [largest_eigenvec_ind_c, ~] = find(eigenval == max(max(eigenval)));
    largest_eigenvec = eigenvec(:, largest_eigenvec_ind_c);

    % Get the largest eigenvalue
    largest_eigenval = max(max(eigenval));

    % Get the smallest eigenvector and eigenvalue
    if(largest_eigenvec_ind_c == 1)
        smallest_eigenval = max(eigenval(:,2));
        smallest_eigenvec = eigenvec(:,2);
    else
        smallest_eigenval = max(eigenval(:,1));
        smallest_eigenvec = eigenvec(1,:);
    end

    % Calculate the angle between the x-axis and the largest eigenvector
    angle = atan2(largest_eigenvec(2), largest_eigenvec(1));

    % This angle is between -pi and pi.
    % Let's shift it such that the angle is between 0 and 2pi
    if angle < 0
        angle = angle + 2*pi;
    end

    % Get the p confidence interval error ellipse
    quantile = chi2inv(confidence, 2) / 2;
    theta_grid = linspace(0, 2*pi, circle_sides);
    phi = angle;
    
    a = quantile * sqrt(largest_eigenval);
    b = quantile * sqrt(smallest_eigenval);

    % the ellipse in x and y coordinates 
    ellipse_x_r  = a*cos( theta_grid );
    ellipse_y_r  = b*sin( theta_grid );

    %Define a rotation matrix
    R = [ cos(phi) sin(phi); -sin(phi) cos(phi) ];

    %let's rotate the ellipse to some angle phi
    r_ellipse = [ellipse_x_r; ellipse_y_r]' * R;

    if nargin < 5
        color = 'red';
        plot_axes = false;
    end
    
    % Draw the error ellipse
    plot(r_ellipse(:,1) + x0, r_ellipse(:,2) + y0,...
            'Color', color,...
            'LineStyle', '-',...
            'LineWidth', 0.5);
    hold on;

    % set(gca,'XGrid','on','YGrid','on');
    
    if nargin < 6
        plot_axes = false;
    end
    
    if plot_axes
        % Plot the eigenvectors
        quiver(x0, y0, largest_eigenvec(1)*sqrt(largest_eigenval), largest_eigenvec(2)*sqrt(largest_eigenval),   '-m', 'LineWidth', 2, 'AutoScale','off');
        quiver(x0, y0, smallest_eigenvec(1)*sqrt(smallest_eigenval), smallest_eigenvec(2)*sqrt(smallest_eigenval), '-g', 'LineWidth', 2, 'AutoScale','off');
        hold on;
    end
end