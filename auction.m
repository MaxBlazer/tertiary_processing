function out = auction(cost)    
    cost_size = size(cost);
    transposed = false;
    
    if cost_size(1) > cost_size(2)
        % auctioners count need to be smaller than objects count
        n = cost_size(2); % auctioners count
        m = cost_size(1); % objects count
        cost = cost';
        transposed = true;
    else
        n = cost_size(1); % auctioners count
        m = cost_size(2); % objects count
    end
    
    e = 1/m; % minimal bid;
    
    assignment = false(size(cost));
    initial_prices = zeros([1 m]);
    prices = initial_prices;
    
    % did_bet(i) = 0 if i have never did bet, 1 otherwise
    have_obj = false([1 n]);
    
    % debug
    % ii = 0;
    % iii = 3;
    
    % first = true;
    
    while true
        % bids(i,j) = bid for j object, made by i auctioner
        bids = NaN(size(cost));
        %{
        if first
            bids(1,2:end) = e;
            first = false;
        end
        %}
        % bidding phase
        for i=1:n                    
            profit = cost(i,:) - prices;
            
            if i == 1
                profit = profit(2:end); % skip dummy obj
                [sorted_profit, sorted_indeces] = sort(profit, 'descend');
                sorted_indeces = sorted_indeces + 1;
                % dummy auctioner must have m-n+(dummy obj owners) optimal objects
                % todo: (dummy obj owners) can be set symmetrically from j=1
                must_have_count = m - n + sum(assignment(:,1));
                must_have_profit = sum(sorted_profit(1:must_have_count)-1.5*e);
                
                if have_obj(i) & (profit*assignment(i,2:end)' >= must_have_profit)
                    continue
                end
                
                % calc and do bid
                for j=1:must_have_count
                    if length(sorted_profit) == j
                        bids(1,sorted_indeces(j)) = 3*cost(1,sorted_indeces(j)); % custom const
                    else
                        % check if already own it
                        if ~assignment(1,sorted_indeces(j))
                            bids(1,sorted_indeces(j)) = cost(1,sorted_indeces(j)) - sorted_profit(j+1) + e;
                        end
                    end
                end
                
                continue
            end
            
            [max_val, j_max] = max(profit);
            
            % proceed (do bid) only if: (1) it's first bid; (2) current bid isn't best            
            if have_obj(i) & (profit*assignment(i,:)' >= max_val-1.5*e)
                continue
            end

            % calc bid
            profit_without_best = profit; % copy
            profit_without_best(j_max) = []; % delete first best profit
            second_best = max(profit_without_best);

            % do bid        
            bids(i,j_max) = cost(i,j_max) - second_best + e;
        end
        
        % debug
        %{
        ii = ii + 1
        
        if ii == iii
            ii
            cost
            return
        end
        %}
        
        % if no bids, success
        if isnan(bids)
            % prices, cost, bids, assignment
            % if cost' out = assignment';
            % ii
            if transposed
                out = assignment';
            else
                out = assignment;
            end
            
            return
        end
        
        % assignment phase
        max_bids = max(bids);
        max_bids(1) = min(bids(:,1)); % dummy obj

        for j=1:m
            % if no bids received, skip
            if isnan(max_bids(j))
                continue
            end

            prices(j) = max_bids(j);
            
            i_winner = NaN; % declaration            
            
            % everyone can own dummy obj
            if j == 1
                i_winner = find(~isnan(bids(:,j))); % find all bidders, regardless of bid value
            else 
                i_loser = find(assignment(:,j)); % assign(:,j) == true
                assignment(i_loser,j) = false; % delete previous owner
                have_obj(i_loser) = false;
                
                i_winner = find(bids(:,j) == max_bids(j));
                i_winner = i_winner(1); % winner is only first bidder
            end
            
            if i_winner ~= 1
                assignment(i_winner,:) = false;
            end
            assignment(i_winner,j) = true; % set new owners
            have_obj(i_winner) = true;
        end
        
        % if dummy auctioner own sufficient number of objects, true
        if sum(assignment(1,2:end)) == m - n + sum(assignment(:,1))
            have_obj(1) = true;
        else
            have_obj(1) = false;
        end
        % debug
        % cost, 
        % bids, prices, assignment
    end    
end